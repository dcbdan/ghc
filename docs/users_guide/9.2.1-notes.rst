.. _release-9-2-1:

Version 9.2.1
==============

Language
~~~~~~~~

* :extension:`ImpredicativeTypes`: Finally, polymorphic types have become first class!
  GHC 9.2 includes a full implementation of the Quick Look approach to type inference for
  impredicative types, as described in in the paper
  `A quick look at impredicativity
  <https://www.microsoft.com/en-us/research/publication/a-quick-look-at-impredicativity/>`__
  (Serrano et al, ICFP 2020).  More information here: :ref:`impredicative-polymorphism`.
  This replaces the old (undefined, flaky) behaviour of the :extension:`ImpredicativeTypes` extension.

Compiler
~~~~~~~~

- New ``-Wredundant-bang-patterns`` flag that enables checks for "dead" bangs.
  For instance, given this program: ::

      f :: Bool -> Bool
      f True = False
      f !x   = x

  GHC would report that the bang on ``x`` is redundant and can be removed
  since the argument was already forced in the first equation. For more
  details see :ghc-flag:`-Wredundant-bang-patterns`.

- New ``-finline-generics`` and ``-finline-generics-aggressively`` flags for
  improving performance of generics-based algorithms.

  For more details see :ghc-flag:`-finline-generics` and
  :ghc-flag:`-finline-generics-aggressively`.

- Type checker plugins which work with the natural numbers now
  should use ``naturalTy`` kind instead of ``typeNatKind``, which has been removed.

``ghc-prim`` library
~~~~~~~~~~~~~~~~~~~~

- ``Void#`` is now a type synonym for the unboxed tuple ``(# #)``.
  Code using ``Void#`` now has to enable :extension:`UnboxedTuples`.

``base`` library
~~~~~~~~~~~~~~~~

- It's possible now to promote the ``Natural`` type: :: 
    
    data Coordinate = Mk2D Natural Natural
    type MyCoordinate = Mk2D 1 10
    
  The separate kind ``Nat`` is removed and now it is just a type synonym for
  ``Natural``. As a consequence, one must enable ``TypeSynonymInstances``
  in order to define instances for ``Nat``.

